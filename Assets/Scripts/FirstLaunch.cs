using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstLaunch : MonoBehaviour
{
    private string[] keys = {"Sound", "Music", "Language" };
    private void Awake()
    {
        foreach (string key in keys)
        {
            if (!PlayerPrefs.HasKey(key))
                PlayerPrefs.SetString(key, "on");
        }
        if (!PlayerPrefs.HasKey("CurrentCharacter"))
            PlayerPrefs.SetString("CurrentCharacter", "Cowboy");
        if (!PlayerPrefs.HasKey("CurrentBackground"))
            PlayerPrefs.SetString("CurrentBackground", "Summer");
        if (PlayerPrefs.GetString("Music") == "off")
            GetComponent<AudioSource>().enabled = false;
        if (!PlayerPrefs.HasKey("WinCount"))
            PlayerPrefs.SetInt("WinCount", 0);
    }
}