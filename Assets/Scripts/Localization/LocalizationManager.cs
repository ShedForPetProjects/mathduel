using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class LocalizationManager : MonoBehaviour
{
    public static int CurrentLanguage { get; private set; }
    private static Dictionary<string, List<string>> localization;
    [SerializeField] private TextAsset textFile;

    public static event LanguageChangeHandler OnLanguageChange;
    public delegate void LanguageChangeHandler();

    private void Awake()
    {
        if (localization == null)
            LoadLocalization();
    }
    private void Start()
    {
        if (PlayerPrefs.GetString("Language") == "on")
            SetLanguage(0);
        else
            SetLanguage(1);
    }
    private void LoadLocalization()
    {
        localization = new Dictionary<string, List<string>>();
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(textFile.text);
        foreach (XmlNode key in xmlDocument["Keys"].ChildNodes)
        {
            string keyStr = key.Attributes["Name"].Value;

            var values = new List<string>();
            foreach (XmlNode translate in key["Translate"].ChildNodes)
                values.Add(translate.InnerText);
            localization[keyStr] = values;
        }
    }

    public static string GetTranslate(string key, int languageId = -1)
    {
        if (languageId == -1)
            languageId = CurrentLanguage;
        if (localization.ContainsKey(key))
            return localization[key][languageId];
        return key;
    }

    public void SetLanguage(int id)
    {
        CurrentLanguage = id;
        OnLanguageChange?.Invoke();
    }
}
