using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class AdController : MonoBehaviour, IUnityAdsListener //IUnityAdsLoadListener, IUnityAdsShowListener
{
    [SerializeField] CoinsManager coinsManager;
    private string actualDate;

    private void Awake()
    {
        if (Advertisement.isSupported)
            Advertisement.Initialize("4400669", false);
        Advertisement.AddListener(this);

        actualDate = System.DateTime.Now.ToString("dd/MM/yyyy");
        if (PlayerPrefs.GetString("LastDateAdShowed") != actualDate)
            GetComponent<Button>().interactable = true;
    }
    public void ShowAd()
    {
        if (Advertisement.IsReady("Rewarded_Android"))
            Advertisement.Show("Rewarded_Android");
    }

    public void OnUnityAdsReady(string placementId) { }

    public void OnUnityAdsDidError(string message) { }

    public void OnUnityAdsDidStart(string placementId) { }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (placementId == "Rewarded_Android" && showResult == ShowResult.Finished)
        {
            coinsManager.AddCoins(5);
            PlayerPrefs.SetString("LastDateAdShowed", actualDate);
            GetComponent<Button>().interactable = false;
        }
    }
    private void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }
}