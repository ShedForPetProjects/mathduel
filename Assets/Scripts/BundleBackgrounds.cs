using UnityEngine;

[CreateAssetMenu(fileName = "New BackgroundsBundle", menuName = "Backgrounds Bundle", order = 30)]
public class BundleBackgrounds : ScriptableObject
{
    [SerializeField]
    private Sprite[] backgrounds;

    public Sprite[] Backgrounds => backgrounds;
}
