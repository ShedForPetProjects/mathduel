using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    [SerializeField] private float timerValue;
    private float secondLeft;
    private Text timerText;
    private CalcManager calcManager;
    public int ShotCount { get; set; }

    void Start()
    {
        timerText = transform.GetChild(4).gameObject.GetComponent<Text>();
        calcManager = GetComponent<CalcManager>();
        secondLeft = timerValue;
        ShotCount = 0;
    }

    private void Update()
    {
        secondLeft -= Time.deltaTime;
        if (secondLeft < 0 || ShotCount == 2)
        {
            secondLeft = timerValue;
            calcManager.Calculate();
            ShotCount = 0;
        }
        timerText.text = Mathf.Round(secondLeft).ToString();
    }
}