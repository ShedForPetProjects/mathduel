using UnityEngine;
using UnityEngine.UI;

public class StatisticsManager : MonoBehaviour
{
    [SerializeField] Text[] statisticsValueText;
    private int playerShots, enemyShots, playerHits, enemyHits;
    private float playerAccuracy, enemyAccuracy;

    public void ShowStatistics()
    {
        playerAccuracy = (float)System.Math.Round(((float)playerHits / (float)playerShots) * 100,2) ;
        enemyAccuracy = (float)System.Math.Round(((float)enemyHits / (float)enemyShots) * 100,2);
        statisticsValueText[0].text = playerShots.ToString();
        statisticsValueText[1].text = enemyShots.ToString();
        statisticsValueText[2].text = playerHits.ToString();
        statisticsValueText[3].text = enemyHits.ToString();
        statisticsValueText[4].text = playerAccuracy.ToString() + "%";
        statisticsValueText[5].text = enemyAccuracy.ToString() + "%";
    }

    public void gettingValues(bool isEnemy, int stateShot)
    {
        if (isEnemy)
        {
            enemyShots++;
            if (stateShot == 1)
                enemyHits++;
        }
        else
        {
            playerShots++;
            if (stateShot == 1)
                playerHits++;
        }
    }
}