using UnityEngine;
using UnityEngine.UI;

public class HPController : MonoBehaviour
{
    [SerializeField] private EndMatchController endMatchController;
    private Slider slider;
    private Text valueHPText;
    private void Start()
    {
        slider = GetComponent<Slider>();
        valueHPText = transform.GetChild(2).GetComponent<Text>();
    }
    public void ChangeHPValue()
    {
        slider.value--;
        valueHPText.text = slider.value.ToString() + "/" + slider.maxValue.ToString();
        if (slider.value == 0)
        {
            transform.GetChild(1).gameObject.SetActive(false);
            endMatchController.ShowScreen();
        }  
    }
}
