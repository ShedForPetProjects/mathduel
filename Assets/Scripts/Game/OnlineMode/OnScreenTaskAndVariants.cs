using UnityEngine;
using UnityEngine.UI;

public class OnScreenTaskAndVariants : MonoBehaviour
{
    private Text[] variantButtonTexts;
    private Button[] variantButtons;
    private Text taskText;
    private readonly char[] operates = new char[] { '+', '-', '*', ':' };

    private void Start()
    {
        variantButtonTexts = transform.GetChild(2).gameObject.GetComponentsInChildren<Text>();
        variantButtons = transform.GetChild(2).gameObject.GetComponentsInChildren<Button>();
        taskText = transform.GetChild(3).gameObject.GetComponent<Text>();
    }
    public void OutputTaskAndVariants(int operate, int argument1, int argument2, float result)
    {
        foreach (Button variantButton in variantButtons)
            variantButton.interactable = true;
        if (argument2 < 0)
            taskText.text = argument1.ToString() + operates[operate] + "(" + argument2.ToString() + ")" + "=";
        else
            taskText.text = argument1.ToString() + operates[operate] + argument2.ToString() + "=";
        foreach (Text variantButtonText in variantButtonTexts)
        {
            switch (operate)
            {
                case 0:
                    variantButtonText.text = Random.Range(-199, 200).ToString();
                    break;
                case 1:
                    variantButtonText.text = Random.Range(-199, 100).ToString();
                    break;
                case 2:
                    variantButtonText.text = Random.Range(-892, 893).ToString();
                    break;
                case 3:
                    variantButtonText.text = System.Math.Round(Random.Range(-99.99f, 100.00f), 1).ToString();
                    break;
            }
        }
        variantButtonTexts[Random.Range(0, 9)].text = System.Math.Round(result, 2).ToString();
    }

    public void DisableVariantButton()
    {
        foreach (Button variantButton in variantButtons)
            variantButton.interactable = false;
    }
}
