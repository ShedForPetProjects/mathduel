using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ShowDamageInfo : MonoBehaviour
{
    private Text damageInfoText;
    public string text { get; set; }
    private void Start()
    {
        GetComponent<LocalizedText>().Localize(text);
        damageInfoText = GetComponent<Text>();
        damageInfoText.color = new Color(1, 0, 0, 1);
        damageInfoText.DOFade(0, 1.5f);
        transform.DOLocalMoveY(-225, 1.5f);
    }

    private void OnDestroy()
    {
        damageInfoText.DOKill();
        transform.DOKill();
    }
}
