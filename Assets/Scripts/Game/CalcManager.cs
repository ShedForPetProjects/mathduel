using UnityEngine;

public class CalcManager : MonoBehaviour
{
    [SerializeField] bool isOnlineMode;
    private ShotManager shotManager;
    private ShotManagerTraining shotManagerTraining;
    private float result;
    private int operate, argument1, argument2;
    private OnScreenTaskAndVariants onScreenTaskAndVariants;

    private void Start()
    {
        if (isOnlineMode)
            shotManager = GetComponent<ShotManager>();
        else
            shotManagerTraining = GetComponent<ShotManagerTraining>();
        onScreenTaskAndVariants = GetComponent<OnScreenTaskAndVariants>();
        Calculate();
    }

    public void Calculate()
    {
        operate = Random.Range(0, 4);
        argument1 = Random.Range(-99, 100);
        if (operate > 1)
            argument2 = Random.Range(-9, 10);
        else
            argument2 = Random.Range(-100, 100);

        if (argument2 == 0)
            argument2 = argument1;

        switch (operate)
        {
            case 0:
                result = argument1 + argument2;
                break;
            case 1:
                result = argument1 - argument2;
                break;
            case 2:
                result = argument1 * argument2;
                break;
            case 3:
                result = (float)System.Math.Round((float) argument1 / argument2, 1);
                break;
        }
        onScreenTaskAndVariants.OutputTaskAndVariants(operate, argument1, argument2, result);
    }

    public void CheckResult(string answer)
    {
        if (System.Convert.ToSingle(answer) == result)
        {
            //������� � ����
            if (isOnlineMode)
                shotManager.Shot(1, false); 
            else
                shotManagerTraining.Shot(1);
        }

        else
        {
            //������
            if (isOnlineMode)
                shotManager.Shot(0, false);
            else
                shotManagerTraining.Shot(0);
        }
            onScreenTaskAndVariants.DisableVariantButton();
    }
}