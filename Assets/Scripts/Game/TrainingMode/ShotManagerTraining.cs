using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShotManagerTraining : MonoBehaviour
{
    [SerializeField] private AnimationController enemyAnimation;
    [SerializeField] private GameObject damageInfoPrefab;
    private GameObject damageInfo;
    private TimerController timerController;
    private List<GameObject> previousDamageInfo = new List<GameObject>();
    public AnimationController playerAnimation { get; set; }
    private void Start()
    {
        timerController = GetComponent<TimerController>();
        timerController.enabled = true;
    }

    public void Shot(int stateShot)
    {
        timerController.ShotCount += 2;
        switch (stateShot)
        {
            case 0:
                StartCoroutine(OnCompleteAttackAnimation("Miss", stateShot));
                break;
            case 1:
                StartCoroutine(OnCompleteAttackAnimation("Damaged", stateShot));
                break;
        }
        playerAnimation.AttackAnimation();
    }

    private IEnumerator OnCompleteAttackAnimation(string text, int stateShot)
    {
        yield return new WaitForSeconds(playerAnimation.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length / 25);
        if (previousDamageInfo.Count > 9)
        {
            previousDamageInfo.RemoveAt(0);
            Destroy(previousDamageInfo[0]);
        }
        damageInfo = Instantiate(damageInfoPrefab, transform.GetChild(1));
        damageInfo.GetComponent<ShowDamageInfo>().text = text;
        previousDamageInfo.Add(damageInfo);
        if (stateShot > 0)
            enemyAnimation.AttackedAnimation();
    }
}