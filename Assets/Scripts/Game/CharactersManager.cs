using UnityEngine;

public class CharactersManager : MonoBehaviour
{
    [SerializeField] private BundleCharacters charactersBundle;
    [SerializeField] private ShotManager shotManager;
    [SerializeField] private ShotManagerTraining shotManagerTraining;
    [SerializeField] private bool isOnlineMode;
    private GameObject character;
    private void Start()
    {
        addCharacterOnScene(false, PlayerPrefs.GetString("CurrentCharacter"));
    }

    public void addCharacterOnScene(bool isEnemy, string nameCharacter)
    {
        for (int i = 0; i < charactersBundle.Characters.Length; i++)
            if (charactersBundle.Characters[i].name == nameCharacter)
                character = Instantiate(charactersBundle.Characters[i]);
        if (isEnemy)
        {
            character.transform.SetParent(transform.GetChild(1).transform);
            character.transform.localScale = new Vector3(-1f, 1f, 1f);
            shotManager.enemyAnimation = character.GetComponent<AnimationController>();
            character.transform.Find("accessory").GetComponent<SpriteRenderer>().color = new Color(0, 0, 0.8f);
        }
        else
        {
            character.transform.SetParent(transform.GetChild(0).transform);
            if (isOnlineMode)
            {
                shotManager.playerAnimation = character.GetComponent<AnimationController>();
                character.transform.Find("accessory").GetComponent<SpriteRenderer>().color = new Color(0.8f, 0, 0);
            }
            else
            {
                shotManagerTraining.playerAnimation = character.GetComponent<AnimationController>();
            }
        }
    }
}