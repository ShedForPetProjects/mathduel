using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine;
using UnityEngine.UI;

public class GooglePlayServicesManager : MonoBehaviour
{
    private const string leaderBoard = "CgkIhoXw-OwEEAIQAQ";
    [SerializeField] Button showLeaderBoardButton;

    private void Start()
    {
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate(success =>
        {
            if (success) 
            {
                showLeaderBoardButton.interactable = true;
            }
            else 
            {
                showLeaderBoardButton.interactable = false;
            }
        });
    }
    private void OnEnable()
    {
        if (showLeaderBoardButton.interactable)
            Social.ReportScore(PlayerPrefs.GetInt("WinCount"), leaderBoard, (bool success) => { });
    }

    public void ShowLeaderBoard()
    {
        Social.ShowLeaderboardUI();
    }

    public void ExitFromGPS()
    {
        PlayGamesPlatform.Instance.SignOut();
    }
}
