using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CharactersBundle", menuName = "Characters Bundle", order = 30)]
public class BundleCharacters : ScriptableObject
{
    [SerializeField]
    private GameObject[] characters;

    public GameObject[] Characters => characters;
}
