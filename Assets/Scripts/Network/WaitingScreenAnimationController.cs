using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class WaitingScreenAnimationController : MonoBehaviour
{
    [SerializeField] Text numberOfVictories;
    private void Start()
    {
        numberOfVictories.text = PlayerPrefs.GetInt("WinCount").ToString();
    }
    public void HideScreen()
    {
        transform.GetChild(0).GetComponent<Image>().DOFade(0, 1);
        transform.GetChild(0).GetComponentInChildren<Text>().DOFade(0, 1);
        transform.GetChild(1).GetComponent<Image>().DOFade(0, 1);
        transform.GetChild(1).GetComponentInChildren<Text>().DOFade(0, 1);
        StartCoroutine("Deactivate");
    }
    
    private IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }
}
