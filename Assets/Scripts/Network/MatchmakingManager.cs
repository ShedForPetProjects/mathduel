using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class MatchmakingManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private Button playButton;
    [SerializeField] private LocalizedText text;

    private void Start()
    {
        PhotonNetwork.NickName = "Player " + Random.Range(0, 10).ToString();
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.ConnectUsingSettings();
        text.Localize("SystemMessage0");
    }

    public override void OnConnectedToMaster()
    {
        text.Localize("SystemMessage1");
        playButton.interactable = true;
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        text.Localize("SystemMessage2");
        playButton.interactable = false;
    }

    private void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 2;
        PhotonNetwork.CreateRoom(null, roomOptions, null);
    }

    public void QuickMatch()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        CreateRoom();
    }

    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel(1);
    }
}