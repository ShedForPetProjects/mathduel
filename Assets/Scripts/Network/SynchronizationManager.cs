using Photon.Pun;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Realtime;

public class SynchronizationManager : MonoBehaviourPunCallbacks
{
    private Text nickPlayer;
    private CalcManager calcManager;
    private GameObject waitingScreen;
    [SerializeField] GameObject dataExchangerPref;
    private void Start()
    {
        nickPlayer = transform.GetChild(5).GetComponent<Text>();
        waitingScreen = transform.GetChild(8).gameObject;
        calcManager = GetComponent<CalcManager>();
        nickPlayer.text = PhotonNetwork.NickName;
        waitingScreen.SetActive(true);
    }

    public void LeaveMatch()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel(0);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        calcManager.enabled = true;
        waitingScreen.GetComponent<WaitingScreenAnimationController>().HideScreen();
        PhotonNetwork.Instantiate(dataExchangerPref.name, new Vector3(0, 0, 0), Quaternion.identity);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") + 1);
        LeaveMatch();
    }

    public override void OnJoinedRoom()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            calcManager.enabled = true;
            waitingScreen.GetComponent<WaitingScreenAnimationController>().HideScreen();
            PhotonNetwork.Instantiate(dataExchangerPref.name, new Vector3(0, 0, 0), Quaternion.identity);
        }
    }
}