using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class DataExchangeManager : MonoBehaviour
{
    private ShotManager shotManager;
    private PhotonView photonView;
    private void Awake()
    {
        transform.SetParent(GameObject.Find("Canvas").transform);
    }
    private void Start()
    {
        shotManager = GetComponentInParent<ShotManager>();
        photonView = PhotonView.Get(gameObject);
        if (photonView.IsMine)
        {
            photonView.RPC("Nick", RpcTarget.All, PhotonNetwork.NickName);
            photonView.RPC("Character", RpcTarget.All, PlayerPrefs.GetString("CurrentCharacter"));
            shotManager.MyDataExchanger = GetComponent<DataExchangeManager>();
        }
     }
    [PunRPC]
    public void Nick(string nickEnemy)
    {
        if (!GetComponent<PhotonView>().IsMine)
            GameObject.Find("NickEnemy").GetComponent<Text>().text = nickEnemy;
    }

    [PunRPC]
    public void Character(string characterName)
    {
        if (!GetComponent<PhotonView>().IsMine)
            GameObject.Find("Characters").GetComponent<CharactersManager>().addCharacterOnScene(true, characterName);
    }

    public void OnShot(int stateShot)
    {
        if (photonView.IsMine)
            photonView.RPC("Shot", RpcTarget.All, stateShot);
    }

    [PunRPC]
    public void Shot(int stateShot)
    {
        if (!GetComponent<PhotonView>().IsMine)
            shotManager.Shot(stateShot, true);    
    }
}