using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayButtonsAnimationController : MonoBehaviour
{
    private List<Transform> playButtons = new List<Transform>();
    private bool isOpen = false;

    private void Start()
    {
        playButtons.AddRange(GetComponentsInChildren<Transform>(true));
        playButtons.RemoveAt(0);
    }
    public void OpenPlay()
    {
        if (isOpen == false)
        {
            foreach (Transform button in playButtons)
            {
                button.GetComponent<Image>().DOFade(1, 1);
                button.GetComponent<Image>().raycastTarget = true;
            }
            playButtons[0].DOLocalMoveX(-350, 1);
            playButtons[1].DOLocalMoveX(350, 1);
            isOpen = true;
        }
        else
        {
            playButtons[0].DOLocalMoveX(0, 1);
            playButtons[1].DOLocalMoveX(0, 1);
            foreach (Transform button in playButtons)
            {
                button.GetComponent<Image>().DOFade(0, 1);
                button.GetComponent<Image>().raycastTarget = false;
            }
            isOpen = false;
        }
    }

    private void OnDestroy()
    {
        DOTween.KillAll();
    }
}
