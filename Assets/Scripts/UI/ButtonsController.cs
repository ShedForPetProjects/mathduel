using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsController : MonoBehaviour
{
    //play buttons
    public void PlayOnline(MatchmakingManager matchmakingManager)
    {
        matchmakingManager.QuickMatch();
    }
    public void PlayOffline()
    {
        SceneManager.LoadScene(2);
    }

    //other buttons
    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
        PhotonNetwork.Disconnect();
    }
    public void QuitGame()
    {
        GetComponent<GooglePlayServicesManager>().ExitFromGPS();
        Application.Quit();
    }
    public void Rate()
    {
        Application.OpenURL("market://details?id=com.SWG.MathDuel");
    }
    public void OpenShop()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(true);
    }

    public void CloseShop()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(false);
    }
}