using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyingController : MonoBehaviour
{
    [SerializeField] private LocalizedText byingStatus;
    [SerializeField] private Text countCoinsText;
    [SerializeField] private int price;
    [SerializeField] private bool isCharacter;
    private string prefsString;
    private int countCoins;
    private Text priceText;
    void Start()
    {
        //����� ���������� �����
        countCoins = PlayerPrefs.GetInt("Coins");

        priceText = transform.GetChild(2).GetComponent<Text>();
        priceText.text = price.ToString();
        //���������� ������ � ������������
        if (isCharacter)
            prefsString = "CurrentCharacter";
        else
            prefsString = "CurrentBackground";
        if (PlayerPrefs.GetInt(this.name) == 1)
        {
            priceText.text = "-";
            byingStatus.Localize("BuyingStatus1");
        }
        if (PlayerPrefs.GetString(prefsString) == this.name)
            byingStatus.Localize("BuyingStatus2");
    }
    public void BuyOrSelectButton()
    {
        int CharacterByingStatus = PlayerPrefs.GetInt(this.name);
        if (CharacterByingStatus == 0)
        {
            if (countCoins >= price)
            {
                countCoins -= price;
                PlayerPrefs.SetInt("Coins", countCoins);
                countCoinsText.text = countCoins.ToString();
                PlayerPrefs.SetInt(this.name, 1);
                byingStatus.Localize("BuyingStatus1");
                priceText.text = "-";
            }
        }
        else if (CharacterByingStatus == 1)
        {
            GetComponentInParent<BuyButtonsController>().ChangeSelectedButton(GetComponentInChildren<Button>());
            PlayerPrefs.SetString(prefsString, this.name);
            byingStatus.Localize("BuyingStatus2");
        }
    }
}