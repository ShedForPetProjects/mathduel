using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyButtonsController : MonoBehaviour
{
    private Button[] buyButtons;
    private Button selectedButton;
    [SerializeField] bool isCharacters;
    private string prefsString;

    private void Start()
    {
        if (isCharacters)
            prefsString = "CurrentCharacter";
        else
            prefsString = "CurrentBackground";
        buyButtons = GetComponentsInChildren<Button>();
        foreach (Button button in buyButtons)
            if (PlayerPrefs.GetString(prefsString) == button.transform.parent.name)
                    selectedButton = button;
        selectedButton.interactable = false;
    }

    public void ChangeSelectedButton(Button newSelectedButton)
    {
        selectedButton.interactable = true;
        selectedButton.GetComponentInChildren<LocalizedText>().Localize("BuyingStatus1");
        selectedButton = newSelectedButton;
        selectedButton.interactable = false;
    }
}