using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    [SerializeField] private AudioClip shotSound;
    [SerializeField] private AudioClip damageSound;
    private AudioSource audioSource;
    private bool enableSound;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (PlayerPrefs.GetString("Sound") == "on")
            enableSound = true;
        else
            enableSound = false;
    }
    public void PlaySound(bool isShot)
    {
        if (enableSound)
        {
            if (isShot)
                audioSource.clip = shotSound;
            else
                audioSource.clip = damageSound;
            audioSource.Play();
        }
    }    
}
