using UnityEngine;

public class AnimationController : MonoBehaviour
{
    private Animator animator;
    private SoundController soundController;

    private void Start()
    {
        animator = GetComponent<Animator>();
        soundController = GetComponent<SoundController>();
    }
    public void AttackedAnimation()
    {
        animator.SetInteger("State", -1);
        soundController.PlaySound(false);
    }
    public void AttackAnimation()
    {
        animator.SetInteger("State", 1);
        soundController.PlaySound(true);
    }
    public void IdleAnimation()
    {
        animator.SetInteger("State", 0);
    }
    public void VictoryAnimation()
    {
        animator.SetInteger("State", 10);
    }
    public void DefeatAnimation()
    {
        animator.SetInteger("State", -10);
    }
}