## Описание скриптов
+ CharactersComponent - контроллеры компонентов персонажей
  + AnimationController.cs - контроллер анимаций
  + SoundController.cs - контроллер звуков
+ Localization - локализация текта
  + LocalizationManager.cs - менеджер локализации
  + LocalizedText.cs - объект локализации (текст)
+ Network - сетевое взаимодействие
  + DataExchangeManager.cs - менеджер обмена данными клиента
  + MatchmakingManager.cs - менеджер матчмейкинга клиентов
  + SynchronizationManager.cs - менеджер синхронизации клиентов
  + WaitingScreenAnimationController.cs - контроллер анимации окна матчмейкинга
+ Online mode - мультиплеер
+ Shop - магазин внутриигровых покупок
  + BuyButtonsController.cs - обработчик кнопки покупки
  + BuyingController.cs - контроллер покупок
  + CoinsManager.cs - менеджер управления внутриигровой валютой
  + IAPManager.cs - менеджер внутриигровых покупок за реал
+ Training mode - режим локальной игры / тренировки
+ UI - контроллеры UI элементов
  + ButtonsController.cs - кнопки меню
  + LocalizationButtonController.cs - кнопки переключения языка
  + PlayButtonsAnimationController.cs - анимации кнопок игровых режимов
  + SettingsButtonController.cs - кнопки настроек
  + SettingsButtonsAnimationController.cs - анимации кнопок настроек
  + SwitchShopButtonController.cs - кнопки переключения вида магазина
  + VariantButtonsController.cs - кнопки с вариантами ответов
+ AdController.cs - контроллер рекламы
+ BackgroundsManager.cs - менеджер фоновых изображений
+ BundleBackgrounds.cs - ScriptableObject набора фоновых изображений
+ BundleCharacters.cs - ScriptableObject набора скинов персонажей
+ FirstLaunch.cs - инициализация настроек при первом запуске
+ GooglePlayServicesManager.cs - менеджер сервисов google play
+ MusicController.cs - контроллер воспроизведения музыки
+ ShowDamageInfo.cs - контроллер показа информации об уроне
